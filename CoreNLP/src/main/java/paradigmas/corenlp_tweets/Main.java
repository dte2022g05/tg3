package paradigmas.corenlp_tweets;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

public class Main {

    public static void main(String[] args) throws IOException {
        
        Pipeline pipeline = new Pipeline();
        pipeline.init();
        String elementos ="";
        
        List<String> tweets = LeerCSV.getTweets("C:\\Users\\docto\\OneDrive\\Escritorio\\Universidad\\Tercer-Curso\\Segundo cuatri\\Tecnologias emergentes\\LAB\\Trabajo\\TG3\\elonmusk_tweets.csv");
        
        System.out.println("\n");
        for (String tweet : tweets){   
            pipeline.estimatingSentiment(tweet);
            System.out.println("--------------------------------------------------------------------------");
            elementos = elementos + "\n"+ pipeline.estimatingSentiment(tweet);
            
        }
        String ruta = "C:\\Users\\docto\\OneDrive\\Escritorio\\Universidad\\Tercer-Curso\\Segundo cuatri\\Tecnologias emergentes\\LAB\\Trabajo\\TG3\\datos" + ".txt";
        File file = new File(ruta);

        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(elementos);
        bw.close();
        
        // Fuente de Datos
        DefaultPieDataset data = new DefaultPieDataset();
        data.setValue("Positivo",pipeline.getPositivo());
        data.setValue("Negativo",pipeline.getNegativo());
        data.setValue("Neutral", pipeline.getNeutral());

        // Creando el Grafico
        JFreeChart chart = ChartFactory.createPieChart(
         "Ejemplo Rapido de Grafico en un ChartFrame", 
         data, 
         true, 
         true, 
         false);

        // Mostrar Grafico
        ChartFrame frame = new ChartFrame("JFreeChart", chart);
        frame.pack();
        frame.setVisible(true);
        
    }

}
