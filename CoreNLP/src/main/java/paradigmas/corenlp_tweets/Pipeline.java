package paradigmas.corenlp_tweets;



import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

import java.util.Properties;

public class Pipeline {
    
    StanfordCoreNLP pipeline = null;
    int positivo =0;
    int negativo =0;
    int neutral =0;
    public  void init(){
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
        pipeline = new StanfordCoreNLP(props);
    }

    public int getPositivo() {
        return positivo;
    }

    public void setPositivo(int positivo) {
        this.positivo = positivo;
    }

    public int getNegativo() {
        return negativo;
    }

    public void setNegativo(int negativo) {
        this.negativo = negativo;
    }

    public int getNeutral() {
        return neutral;
    }

    public void setNeutral(int neutral) {
        this.neutral = neutral;
    }
    
    
    
    public String estimatingSentiment(String texto){
        int sentimentInt;
        String elemento = "";
        String sentimentName;
        Annotation annotation = pipeline.process(texto);
        
        double contadorSentimientos = 0;
        int numeroFrases = annotation.get(CoreAnnotations.SentencesAnnotation.class).size();
        int contador = 1;
        if(numeroFrases > 1) System.out.println(texto);
        
        for(CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)){
            Tree tree = sentence.get(SentimentCoreAnnotations.SentimentAnnotatedTree.class);
            sentimentInt = RNNCoreAnnotations.getPredictedClass(tree);
            sentimentName = sentence.get(SentimentCoreAnnotations.SentimentClass.class);
            System.out.println(sentimentName + ": " + sentimentInt + " ---- " + sentence);
            elemento = elemento + "\n"+ sentimentName + ": " + sentimentInt + " ---- " + sentence;
            if (sentimentInt == 1){
                positivo =positivo + 1;
            }
            if (sentimentInt == 2){
                negativo =negativo + 1;
            }
            if (sentimentInt == 3){
                neutral =neutral + 1;
            }
            contadorSentimientos += sentimentInt;
            if(numeroFrases > 1 && contador == numeroFrases){
                System.out.println("Valor medio sentimiento: "+contadorSentimientos/numeroFrases);
                elemento = elemento + "\n" + "Valor medio sentimiento: "+contadorSentimientos/numeroFrases;
            }
            contador++;
            
        }
        
      return elemento;  
    }
}
