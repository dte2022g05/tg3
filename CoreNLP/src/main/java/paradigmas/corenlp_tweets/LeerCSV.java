package paradigmas.corenlp_tweets;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LeerCSV {
    
    public static List<String> getTweets(String archivo){
        List<List<String>> tweets = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                tweets.add(Arrays.asList(values));
            }
            
            List<String> tweetsFormateados = new ArrayList<>();
            for (int i=1; i<tweets.size(); i++){
                tweetsFormateados.add(tweets.get(i).get(2).substring(1, tweets.get(i).get(2).length()-1));
            }
            
            return tweetsFormateados;
            
        } catch (IOException ex) {
            System.out.println("Error bufferedReader al leer el csv");
        }
        return new ArrayList<>();
    }
}
