#!/usr/bin/env Python3
from email.quoprimime import quote
from hashlib import new
from itertools import count
import json
from re import I
from statistics import mode
import tweepy
import configobj
import spacy
import csv
    
# API keyws that yous saved earlier
api_key = "5NdJUZ9ZpsifhZAEIPWmwLequ"
api_secrets = "mj41PaXFZ7rYKWV3gYuqHWEhbCAUvM4orZKkOSQ1Pa2EQZVbjI"
access_token = "4778194457-MUhreyFA1WgvueOePInI2iKu3BgiEztf4hnrBw9"
access_secret = "wxW6i6nr3cLhsQxDmAKqnW1uTKj8AEGsHKHdj4hMpan46"
 
#Remove unprinted chars and line jumps from the text

def strip_undesired_chars(tweet):
    stripped_tweet = tweet.replace('\n', ' ').replace('\r', '')
    char_list = [stripped_tweet[j] for j in range(len(stripped_tweet)) if ord(stripped_tweet[j]) in range(65536)]
    stripped_tweet=''
    for j in char_list:
        stripped_tweet=stripped_tweet+j
    return stripped_tweet

#Get last 3240 tweets max

def get_all_tweets(screen_name):
    limit_number = 3240
    # Authenticate to Twitter
    auth = tweepy.OAuthHandler(api_key,api_secrets)
    auth.set_access_token(access_token,access_secret)
    api = tweepy.API(auth)
    try:
        api.verify_credentials()
        print('Successful Authentication')
    except:
        print('Failed authentication')
    #init list of tweets 
    alltweets = []
    #Request of 200 more recent tweets, 200 is the max allowed 
    new_tweets = api.user_timeline(screen_name = screen_name, count = 200)
    #Save tweets 
    alltweets.extend(new_tweets)
    #Oldest tweet id -1
    oldest = alltweets[-1].id -1
    #Array iteration
    while len(new_tweets) > 0 and len(alltweets) <= limit_number:
        print ("Getting tweets before" + str(oldest))
        new_tweets = api.user_timeline(screen_name=screen_name,count = 200 , max_id = oldest)
        alltweets.extend(new_tweets)
        #Update ID
        oldest = alltweets[-1].id -1
        #State
        print (str(len(alltweets)) + "Downloades tweets at the moment")
        #2D array for .csv
        outtweets = [(tweet.id_str, tweet.created_at, strip_undesired_chars(tweet.text),tweet.retweet_count,
        str(tweet.favorite_count)+'') for tweet in alltweets]
        #Write on CSV
        with open('%s_tweets.csv' % screen_name, "w", newline='') as f:       
            writer = csv.writer(f, quoting=csv.QUOTE_ALL)
            writer.writerow(['id','created_at','text','retweet_count','favorite_count'''])
            writer.writerows(outtweets)    
        pass

if __name__ == '__main__':
    #Input the Twitter user
    get_all_tweets("djmariio")

