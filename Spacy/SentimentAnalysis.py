from asyncore import write
import sys,tweepy,csv,re
from tkinter.ttk import Labelframe
from textblob import TextBlob
import matplotlib.pyplot as plt
import spacy
from time import sleep
from tqdm import tqdm 



class SentimentAnalysis:

    def __init__(self):
        self.tweets = []
        self.tweetText = []

    def DownloadData(self):
        # authenticating
        consumerKey = '5NdJUZ9ZpsifhZAEIPWmwLequ'
        consumerSecret = 'mj41PaXFZ7rYKWV3gYuqHWEhbCAUvM4orZKkOSQ1Pa2EQZVbjI'
        accessToken = '4778194457-MUhreyFA1WgvueOePInI2iKu3BgiEztf4hnrBw9'
        accessTokenSecret = 'wxW6i6nr3cLhsQxDmAKqnW1uTKj8AEGsHKHdj4hMpan46'
        auth = tweepy.OAuthHandler(consumerKey, consumerSecret)
        auth.set_access_token(accessToken, accessTokenSecret)
        api = tweepy.API(auth)

        # input for term to be searched and how many tweets to search
        searchTerm = input("Introduce el usuario que quieres buscar: ")
        NoOfTerms = int(input("¿Cuantos tweets quieres buscar?: "))

        # searching for tweets
        self.tweets = tweepy.Cursor(api.search_tweets, q=searchTerm, lang = "en").items(NoOfTerms)

        # Open/create a file to append data to
        csvFile = open('result.csv', 'a')

        # Use csv writer
        csvWriter = csv.writer(csvFile)


        # creating some variables to store info
        polarity = 0
        positive = 0
        wpositive = 0
        spositive = 0
        negative = 0
        wnegative = 0
        snegative = 0
        neutral = 0

        # iterating through tweets fetched
        for tweet in self.tweets:
            #Append to temp so that we can store in csv later. I use encode UTF-8
            self.tweetText.append(self.cleanTweet(tweet.text).encode('utf-8'))
            # print (tweet.text.translate(non_bmp_map))    #print tweet's text
            analysis = TextBlob(tweet.text)
            # print(analysis.sentiment)  # print tweet's polarity
            polarity += analysis.sentiment.polarity  # adding up polarities to find the average later

            if (analysis.sentiment.polarity == 0):  # adding reaction of how people are reacting to find average later
                neutral += 1
            elif (analysis.sentiment.polarity > 0 and analysis.sentiment.polarity <= 0.3):
                wpositive += 1
            elif (analysis.sentiment.polarity > 0.3 and analysis.sentiment.polarity <= 0.6):
                positive += 1
            elif (analysis.sentiment.polarity > 0.6 and analysis.sentiment.polarity <= 1):
                spositive += 1
            elif (analysis.sentiment.polarity > -0.3 and analysis.sentiment.polarity <= 0):
                wnegative += 1
            elif (analysis.sentiment.polarity > -0.6 and analysis.sentiment.polarity <= -0.3):
                negative += 1
            elif (analysis.sentiment.polarity > -1 and analysis.sentiment.polarity <= -0.6):
                snegative += 1


        # Write to csv and close csv file
        csvWriter.writerow(self.tweetText)
        csvFile.close()

        # finding average of how people are reacting
        positive = self.percentage(positive, NoOfTerms)
        wpositive = self.percentage(wpositive, NoOfTerms)
        spositive = self.percentage(spositive, NoOfTerms)
        negative = self.percentage(negative, NoOfTerms)
        wnegative = self.percentage(wnegative, NoOfTerms)
        snegative = self.percentage(snegative, NoOfTerms)
        neutral = self.percentage(neutral, NoOfTerms)

        # finding average reaction
        polarity = polarity / NoOfTerms
        #Progress bar
        for i in tqdm(range(10)):
            sleep(0.2)
        # printing out data
        print("Como han reaccionado las personas " + searchTerm + " analizando " + str(NoOfTerms) + " tweets.")
        print()
        print("Reporte general: ")

        if (polarity == 0):
            print("Neutral")
        elif (polarity > 0 and polarity <= 0.3):
            print("Un poco positivo")
        elif (polarity > 0.3 and polarity <= 0.6):
            print("Positivo")
        elif (polarity > 0.6 and polarity <= 1):
            print("Muy positivo")
        elif (polarity > -0.3 and polarity <= 0):
            print("Un poco negativo")
        elif (polarity > -0.6 and polarity <= -0.3):
            print("Negativo")
        elif (polarity > -1 and polarity <= -0.6):
            print("Muy negativo")

        print()
        print("Reporte detallado: ")
        print(str(positive) + "% Personas que interactuaron lo hicieron de forma positivo.")
        print(str(wpositive) + "% Personas que interactuaron lo hicieron de forma un poco positivo.")
        print(str(spositive) + "% Personas que interactuaron lo hicieron de forma muy positivo.")
        print(str(negative) + "% Personas que interactuaron lo hicieron de forma negativo.")
        print(str(wnegative) + "% Personas que interactuaron lo hicieron de forma un poco negativo.")
        print(str(snegative) + "% Personas que interactuaron lo hicieron de forma muy negativo.")
        print(str(neutral) + "% Personas que interactuaron lo hicieron de forma neutral.")

        #File creation
        results = open(searchTerm+"Result.txt", "w")
        #File write
        results.write("Reporte detallado de " + searchTerm +":\n")
        results.write(str(positive) + "% Personas que interactuaron lo hicieron de forma positivo.\n")
        results.write(str(wpositive) + "% Personas que interactuaron lo hicieron de forma poco positivo.\n")
        results.write(str(spositive) + "% Personas que interactuaron lo hicieron de forma muy positivo.\n")
        results.write(str(negative) + "% Personas que interactuaron lo hicieron de forma negativo.\n")
        results.write(str(wnegative) + "% Personas que interactuaron lo hicieron de forma un poco positivo.\n")
        results.write(str(snegative) + "% Personas que interactuaron lo hicieron de forma muy negativo.\n")
        results.write(str(neutral) + "% Personas que interactuaron lo hicieron de forma neutral.\n")

        self.plotPieChart(positive, wpositive, spositive, negative, wnegative, snegative, neutral, searchTerm, NoOfTerms)



    def cleanTweet(self, tweet):
        # Remove Links, Special Characters etc from tweet
        return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t]) | (\w +:\ / \ / \S +)", " ", tweet).split())

    # function to calculate percentage
    def percentage(self, part, whole):
        temp = 100 * float(part) / float(whole)
        return format(temp, '.2f')

    def plotPieChart(self, positive, wpositive, spositive, negative, wnegative, snegative, neutral, searchTerm, noOfSearchTerms):
        labels = ['Positivo [' + str(positive) + '%]', 'Un poco positivo [' + str(wpositive) + '%]','Muy positivo [' + str(spositive) + '%]', 'Neutral [' + str(neutral) + '%]',
                  'Negativo [' + str(negative) + '%]', 'Un poco negativo [' + str(wnegative) + '%]', 'Muy negativo [' + str(snegative) + '%]']
        sizes = [positive, wpositive, spositive, neutral, negative, wnegative, snegative]
        colors = ['yellowgreen','lightgreen','darkgreen', 'gold', 'red','lightsalmon','darkred']
        patches, texts = plt.pie(sizes, colors=colors, startangle=90)
        plt.legend(patches, labels, loc="best")
        plt.title('Como reaccionan las personas: ' + searchTerm + ' analizando ' + str(noOfSearchTerms) + ' Tweets.')
        plt.axis('equal')
        plt.tight_layout()
        plt.show()
    



if __name__== "__main__":
    sa = SentimentAnalysis()
    sa.DownloadData()